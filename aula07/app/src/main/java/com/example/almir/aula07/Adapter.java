package com.example.almir.aula07;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends BaseAdapter {

    private String[] candidatos = new String[]{"Bolsonaro", "geraldo alckmin", "Levy fidelix", "Marina", "Ciro"};
    private Context context;

    public Adapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return candidatos.length;
    }

    @Override
    public Object getItem(int position) {
        return candidatos[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String candidato = candidatos[position];

        View view = LayoutInflater.from(context).inflate(R.layout.activity_adapter
                , parent, false);

        TextView t = (TextView) view.findViewById(R.id.twAdapterSimples);
        t.setText(candidato);

        ImageView imageView = (ImageView) view.findViewById(R.id.ivAdapterSimples);
        if (position == 0){
            imageView.setImageResource(R.drawable.bolsonaro);
        }
        if (position == 1){
            imageView.setImageResource(R.drawable.geraldo_alckmin);
        }
        if (position == 2){
            imageView.setImageResource(R.drawable.levy_fidelix);
        }
        if (position == 3){
            imageView.setImageResource(R.drawable.marina);
        }
        if (position == 4){
            imageView.setImageResource(R.drawable.ciro);
        }







        return view;
    }

}
