package com.example.almir.aula07;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class Alerta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerta);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    public void AlertaOk(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.download);
        builder.setTitle("Atenção");
        builder.setMessage("Você vai conhecer sua realidade");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Clicou em OK!", Toast.LENGTH_LONG).show();
                return;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void AlertaSimNão(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.download);
        builder.setTitle("Atenção");
        builder.setMessage("Sua visão de mundo está correta com realidade");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Então conhece o Olavo de Carvalho", Toast.LENGTH_LONG).show();
                return;
            }
        });

        builder.setNegativeButton("Não tenho certeza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(),"Deveria conhecer o Olavo", Toast.LENGTH_LONG).show();
                return;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void AlertaSimNãoCancelar(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.download);
        builder.setTitle("Atenção");
        builder.setMessage("Sua visão de mundo está correta com realidade");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Já leu esse livro!", Toast.LENGTH_LONG).show();
                return;
            }
        });

        builder.setNegativeButton("Não tenho certeza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(),"Leia esse livro meu filho", Toast.LENGTH_LONG).show();
                return;
            }
        });

        builder.setNeutralButton("Volte e permaneça na dúvida", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(),"Vai continuar no mundinho fantasioso", Toast.LENGTH_LONG).show();
                return;
            }
        });




        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
