package com.example.almir.aula07;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class Bar extends AppCompatActivity {

    private static final String TAG = "livro";
    private ProgressBar mProgress;
    private boolean alive = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    mProgress = (ProgressBar) findViewById(R.id.barraProgresso);
    Button b = (Button) findViewById(R.id.btOK);
        b.setOnClickListener(new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            alive = true;
            new Thread(new Runnable() {
                public void run() {
                    for (int i = 0; i <= 100; i++) {
                        if(!alive) {
                            Log.d(TAG, "Fim Progress");
                            break;
                        }
                        final int progress = i;
                        // Atualiza a barra de progresso
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Log.d(TAG, ">> Progress: " + progress);
                                mProgress.setProgress(progress);
                            }
                        });
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.i(TAG, "Fim.");
                }
            }).start();
        }
    });
}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        alive = false;
    }

    public void PararTarefa(View view) {
        alive =false;
    }

}
