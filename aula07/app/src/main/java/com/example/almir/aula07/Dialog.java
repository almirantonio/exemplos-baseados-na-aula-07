package com.example.almir.aula07;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Dialog extends AppCompatActivity {
    private static final String URLLivro = "https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=5093597&qld=90&l=430&a=-1";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        dialog = ProgressDialog.show(this, "Não seja um", "Buscando imagem, por aguarde...", false, true);
        downloadImagem(URLLivro);
    }
    private void downloadImagem(final String urlImg) {
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(5000);
                    // Faz o download da imagem
                    URL url = new URL(urlImg);
                    InputStream in = url.openStream();
                    // Converte a InputStream do Java para Bitmap
                    final Bitmap imagem = BitmapFactory.decodeStream(in);
                    in.close();
                    // Atualiza a tela
                    atualizaImagem(imagem);
                } catch (IOException e) {
                    // Uma aplica��o real deveria tratar este erro
                    Log.e("Ebro", e.getMessage(), e);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void atualizaImagem(final Bitmap imagem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Fecha a janela de progresso
                dialog.dismiss();
                ImageView imgView = (ImageView) findViewById(R.id.img);
                imgView.setImageBitmap(imagem);
            }
        });
    }
}
